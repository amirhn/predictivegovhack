# Predictive prism

Govhack 17

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Make sure you have node and npm already installed

```
node
npm
```

### Installing

Then install project dependencies 

```
npm install
```

And run

```
npm start
```

## Running the tests

TODO

## Deployment

TODO

## Built With

* NodeJS
* Express
* MongoDB
* JQuery
* Boostrap
* Mapbox
* ...

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

TODO